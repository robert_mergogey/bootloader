# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>

FROM registry.hub.docker.com/library/debian:stable-slim

LABEL Maintainer="Olliver Schinagl <oliver@schinagl.nl>"

RUN apt-get update && \
    apt-get install --no-install-recommends -y \
        bc \
        bison \
        bzip2 \
        dc \
        device-tree-compiler \
        flex \
        gcc \
        gcc-arm-linux-gnueabihf \
        git \
        libc6-dev \
        libc6-dev-armhf-cross \
        libssl-dev \
        lzma \
        lzop \
        make \
        ncurses-dev \
        swig \
        xz-utils \
        zstd \
    && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/

ENV TARGET_ARCH="arm"
ENV CROSS_COMPILE="arm-linux-gnueabihf-"
COPY "./dockerfiles/buildenv_check.sh" "/test/buildenv_check.sh"
