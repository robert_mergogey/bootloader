# ESBS bootloader
This repository contains build scripts to build a bootloader used to bring up
and boot supported boards.

Currently the used bootloader this repository wraps around is
[U-Boot](https://www.denx.de/wiki/U-Boot/).

The build script can be run using the *-h* parameter for more information about
arguments and environment variables.

## Quick getting started guide
To quickly integrate these tools
```console
git init <my_u-boot_project>
cd <my_u-boot_project>
git submodule add u-boot <upstream u-boot URL> u-boot
git submodule add <esbs bootloader URL> esbs
ln -s esbs/docker_build.sh
./docker_build.sh help
```

## Installation
The make use of the bootloader build system a new directory or repository is to
be created including this repository as a sub directory or submodule named
'esbs'. As currently u-boot is the only supported bootloader, u-boot needs to be
added as a sub directory or submodule named 'u-boot'. Finally a configuration
directory holding configuration files. See below for an example tree.
```
bootloader-project
├── configs
├── esbs
└── u-boot
```

Building the bootloader then is as simple as calling the build script from the
esbs sub directory, optionally passing along the config file.
```sh
./esbs/build.sh
```

## Wrapper
While the above example can be even more simplified by adding a symlink to the
main directory, The recommended way is actually to use the supplied docker
container. Included in this repository along side the build script is a script
called `docker_build.sh`. This reduces the hassle of finding and installing the
needed dependencies.

Note that if a registry image is preferred over having the script build its own
is to expose the environment variable **CI_REGISTRY_IMAGE** to the script.
```sh
CI_REGISTRY_IMAGE="some.registry.image:latest" ./docker_build.sh -h
```
Alternatively, the variable can be exported via the shell.

### Prerequisites
To ensure that the included `docker_build.sh` script can run without
problems the script will perform a self-test at start. If there are missing
dependencies, the script will print an error and inform about what was missing.

### Usage
To simplify usage it is recommended to create a symlink in the main
directory. Note that the symlink must keep its name.
```sh
ln -s esbs/docker_build.sh .
./docker_build.sh help
```

## Configuration
The configs directory contains u-boot configuration files. If none is supplied
to the build script via the -b parameter, the first `.config` file will be used.

If no config file exist yet, supplying the BUILD_CONFIG parameter will try to
create one, so it is best to that in combination with "<board>_defconfig".

## Building
After fetching this repository, there are several methods to build U-Boot.
Either a local build, which requires all build requirements are resolved locally,
or using a [Docker](https://www.docker.org) container.
Note, it is also possible to use the gitlab-runner locally as the CI would but
is not supported yet.

## Docker build
Building using docker is probably the easiest solution to achieve reliable
builds. All that is needed for this is a docker image to run the build in,
for which there are two possibilities. Either pull the upstream image or
locally generate it. It is recommended to always pull the upstream image,
unless development also requires changes to the build environment itself.

Simply invoke the following command to run u-boot's 'help' using the upstream
docker image.
```sh
docker run \
       --hostname "$(hostname)" \
       --interactive \
       --rm \
       --tty \
       --volume "$(pwd):/workdir" \
       --workdir "${WORKDIR}" \
       bootloader:dev \
       nice -n 19 ./build.sh help
```
Any standard u-boot command can be supplied to the dockerized `build.sh` script.
As such, calling the `build.sh` script will run make in the u-boot sub directory
passing along any arguments supplied. In other words, to run **_make menuconfig_**
replace 'help' with 'menuconfig' in the previous example.

Note that `nice -n 19` is not needed at all and can be omitted, however on
desktop machines particularly this can make the build process not as interfering.

To generate (a new) docker image to generate a build with, first build it as
is done normally using `docker build -t bootloader:dev .` tagging the image.
Next run the previous _docker run_ command again.
Also the container is uploaded to this repositories registry and can be used
as a tag instead.

### Local build
When doing a build without docker, calling the `build.sh` script will run make
in the u-boot sub directory passing along any arguments supplied. In other words,
to run **_make menuconfig_** use the script as `./build.sh menuconfig`.

There is a simple script available, `buildenv_check.sh` to verify the basic
needs of the build. Note, that when doing a native build (for example in an arm
environment), the **CROSS_COMPILE** environment variable still needs to be set.

The build environment check currently only checks if the cross compiler is valid
and can cross-compile binaries. For other additional requirements check the
Dockerfile.
