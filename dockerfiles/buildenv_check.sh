#!/bin/sh
#
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

set -eu

CROSS_COMPILE="${CROSS_COMPILE?}"
REQUIRED_COMMANDS="
	[
	bc
	bison
	break
	bzip2
	cat
	cd
	command
	cp
	date
	dc
	dirname
	dtc
	echo
	exit
	find
	getopts
	git
	grep
	gzip
	install
	lzma
	lzop
	make
	mkdir
	mktemp
	nice
	nproc
	printf
	readlink
	rm
	rmdir
	shift
	stat
	test
	tr
	trap
	uname
	wc
	which
	xz
	zstd
"


usage()
{
	echo "Usage: ${0} [OPTIONS]"
	echo "A simple test script to verify the environment has all required tools."
	echo "    -h  Print usage"
}

init()
{
	trap cleanup EXIT

	test_dir="$(mktemp -d -p "${TMPDIR:-/tmp}" "buildenv_check.XXXXXX")"
}

cleanup()
{
	if [ -d "${test_dir:?}" ]; then
		rm -rf "${test_dir:?}/"
	fi

	trap EXIT
}

check_requirements()
{
	for cmd in ${REQUIRED_COMMANDS}; do
		if ! test_result="$(command -V "${cmd}")"; then
			test_result_fail="${test_result_fail:-}${test_result}\n"
		else
			test_result_pass="${test_result_pass:-}${test_result}\n"
		fi
	done

	echo "Available commands:"
	# As the results contain \n, we expect these to be interpreted.
	# shellcheck disable=SC2059
	printf "${test_result_pass:-none\n}"
	echo
	echo "Missing commands:"
	# shellcheck disable=SC2059
	printf "${test_result_fail:-none\n}"
	echo

	if [ -n "${test_result_fail:-}" ]; then
		echo "Command test failed, missing programs."
		test_failed=1
	fi
}

check_compiler()
{
	cat <<-EOT > "${test_dir}/compiler_test.c"
		int main(void)
		{
			return 0;
		}
	EOT

	"${CROSS_COMPILE:-}gcc" \
	                        -o "${test_dir}/compiler_test" \
	                        -c "${test_dir}/compiler_test.c" \
	                        -Wall -Werror || compile_failed=1

	"${CROSS_COMPILE:-}objdump" -dS "${test_dir}/compiler_test" 1> /dev/null || compile_failed=1

	printf "compiler support: "
	if [ "${compile_failed:-0}" -ne 0 ]; then
		test_failed=1
		echo "error"
	else
		echo "ok"
	fi
}

main()
{
	while getopts ":h" options; do
		case "${options}" in
		h)
			usage
			exit 0
			;;
		:)
			echo "Option -${OPTARG} requires an argument."
			exit 1
			;;
		?)
			echo "Invalid option: -${OPTARG}"
			exit 1
			;;
		esac
	done
	shift "$((OPTIND - 1))"

	check_requirements
	init
	check_compiler

	if [ "${test_failed:-0}" -ne 0 ]; then
		echo "Test(s) failed."
		exit 1
	fi

	echo "All Ok"

	cleanup
}

main "${@}"

exit 0
