#!/bin/sh
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>

set -eu

DEF_BUILD_SRC="./u-boot"
DEF_KEEP_BUILD_CACHE="false"
DEF_OUTPUT_DIR="output"

e_err() {
	echo >&2 "ERROR: ${*}"
}

e_warn() {
	echo "WARN: ${*}"
}

usage() {
	echo "Uage: ${0} [OPTIONS] [COMMAND]"
	echo "This is a wrapper script around the normal U-Boot Makefile, calling make with [COMMAND]."
	echo "Its purpose is to make setting of build dirs and passing of configurations easier."
	echo "    -a  Set the target architecture, an empty var performs a native build (current: '${TARGET_ARCH:-native}'). [TARGET_ARCH]"
	echo "    -b  Build config directory to use [BUILD_CONFIG]"
	echo "    -c  Use previous build cache (default: ${DEF_KEEP_BUILD_CACHE}) [KEEP_BUILD_CACHE]"
	echo "    -h  Print usage"
	echo "    -o  Set the output directory (default: '${DEF_OUTPUT_DIR}/\${ARCH}/\${BUILD_CONFIG}') [OUTPUT_DIR]"
	echo "    -s  Location of the U-Boot source directory (default: '${DEF_BUILD_SRC}') [BUILD_SRC]"
	echo "    -x  Override the target compiler, an empty var performs a native build (default: native). [CROSS_COMPILE]"
	echo
	echo "All options can also be passed in environment variables (listed between [brackets])."
}

init() {
	trap cleanup EXIT

	if [ -z "${MAKEFLAGS:-}" ]; then
		MAKEFLAGS="-j$(($(nproc) - 1))"
		e_warn "MAKEFLAGS where not set, setting them to '${MAKEFLAGS}'."
		export MAKEFLAGS
	fi

	if [ "${keep_build_cache}" != "true" ] &&
	   [ -d "${build_dir}" ]; then
		rm -rf "${build_dir:?}"
	fi
	mkdir -p "${build_dir}"

	if [ -z "${LOCALVERSION:-}" ] && git rev-parse --show-cdup 2> "/dev/null"; then
		if ! git describe 2> "/dev/null"; then
			LOCALVERSION="g"
		fi
		LOCALVERSION="-${LOCALVERSION:-}$(git describe --always --dirty)"
		export LOCALVERSION
	fi

	if [ -d "${output_dir}" ]; then
		rm -rf "${output_dir:?}"
	fi
	mkdir -p "${output_dir}/boot/"

	uboot_config="$(readlink -f "${build_config}/u-boot.config")"
}

cleanup() {
	if [ "${keep_build_cache}" != "true" ] &&
	   [ -d "${build_dir}" ]; then
		rm -rf "${build_dir:?}"
	fi

	trap EXIT
}

build_uboot() {
	ARCH="${arch}" \
	CROSS_COMPILE="${cross_compile}" \
	nice -n 19 make -C "${build_src}" \
	KCONFIG_CONFIG="${uboot_config}" \
	O="${build_dir}" \
	"${@}"
}

install_uboot() {
	# No 'make install', so build and then manually install
	build_uboot u-boot

	if [ -f "${build_dir}/u-boot-dtb.img" ]; then
		if [ -e "${output_dir}/boot/u-boot.img" ]; then
			mv "${output_dir}/boot/u-boot.img" \
			   "${output_dir}/boot/u-boot.img.old"
		fi

		install -D -m 644 "${build_dir}/u-boot.img" \
		        "${output_dir}/boot/u-boot.img"
	fi

	if [ -f "${build_dir}/spl/u-boot-spl.bin" ]; then
		if [ -e "${output_dir}/boot/u-boot-spl.bin" ]; then
			mv "${output_dir}/boot/u-boot-spl.bin" \
			   "${output_dir}/boot/u-boot-spl.bin.old"
		fi

		install -D -m 644 "${build_dir}/spl/u-boot-spl.bin" \
		        "${output_dir}/boot/u-boot-spl.bin"
	fi

	if [ -f "${build_dir}/spl/boot.bin" ]; then
		if [ -e "${output_dir}/boot/boot.bin" ]; then
			mv "${output_dir}/boot/boot.bin" \
			   "${output_dir}/boot/boot.bin.old"
		fi

		install -D -m 644 "${build_dir}/spl/boot.bin" \
		        "${output_dir}/boot/boot.bin"
	fi

	if [ -f "${build_dir}/MLO" ]; then
		if [ -e "${output_dir}/boot/MLO" ]; then
			mv "${output_dir}/boot/MLO" \
			   "${output_dir}/boot/MLO.old"
		fi

		install -D -m 644 "${build_dir}/MLO" \
		        "${output_dir}/boot/MLO"

		if [ -e "${output_dir}/boot/MLO.byteswap" ]; then
			mv "${output_dir}/boot/MLO.byteswap" \
			   "${output_dir}/boot/MLO.byteswap.old"
		fi

		install -D -m 644 "${build_dir}/MLO.byteswap" \
		        "${output_dir}/boot/MLO.byteswap"
	fi
}

main() {
	_start_time="$(date "+%s")"

	while getopts ":a:b:cho:s:x:" _options; do
		case "${_options}" in
		a)
			arch="${OPTARG}"
			;;
		b)
			build_config="$(echo "${OPTARG}" | sed 's|/*$||g')"
			;;
		c)
			keep_build_cache="true"
			;;
		h)
			usage
			exit 0
			;;
		o)
			output_dir="${OPTARG}"
			;;
		s)
			build_src="${OPTARG}"
			;;
		x)
			cross_compile="${OPTARG}"
			;;
		:)
			e_err "Option -${OPTARG} requires an argument."
			exit 1
			;;
		?)
			e_err "Invalid option: -${OPTARG}"
			exit 1
			;;
		esac
	done
	shift "$((OPTIND - 1))"

	if [ -z "${build_config:=${BUILD_CONFIG:-}}" ]; then
		for config in "configs/"*; do
			if [ -d "${config}" ] && \
			   [ -f "${config}/u-boot.config" ]; then
				build_config="${config}"
				break
			fi
		done
	fi

	arch="${arch:-${TARGET_ARCH:-$(uname -m)}}"
	build_name="${build_config##*/}"
	build_dir="${BUILD_DIR:-$(pwd)/.build/${arch}/${build_name}}"
	build_src="$(readlink -f "${build_src:-${BUILD_SRC:-${DEF_BUILD_SRC}}}")"
	cross_compile="${cross_compile:-${CROSS_COMPILE:-}}"
	keep_build_cache="${keep_build_cache:-${KEEP_BUILD_CACHE:-${DEF_KEEP_BUILD_CACHE}}}"
	output_dir="${output_dir:-${OUTPUT_DIR:-$(pwd)/${DEF_OUTPUT_DIR}/${arch}/${build_name}}}"

	if [ ! -d "${build_config}" ]; then
		e_err "Missing build configuration directory."
		e_err "Please supply a valid one via BUILD_CONFIG."
		usage
		exit 1
	fi

	init

	case "${@}" in
	*help*)
		build_uboot "help"
		cleanup
		exit 0
		;;
	*menuconfig*)
		build_uboot "menuconfig"
		cleanup
		exit 0
		;;
	*) ;;

	esac

	echo "Building for target '${build_name}'."
	build_uboot "${@}"

	echo "Installing into '${output_dir}'."
	install_uboot

	echo "==============================================================================="
	echo "Build report for $(date -u)"
	if [ -f "${build_dir}/include/config/uboot.release" ]; then
		echo "Version: U-Boot $(tr -d '"' < "${build_dir}/include/config/uboot.release")"
	fi
	echo
	if [ -f "${output_dir}/boot/u-boot-spl.bin" ]; then
		echo "U-Boot SPL size:	$(du -b -d 0 -h "$(readlink -f "${output_dir}/boot/u-boot-spl.bin")" | cut -f1)"
	fi
	if [ -f "${output_dir}/boot/u-boot.img" ]; then
		echo "U-Boot proper size:	$(du -b -d 0 -h "${output_dir}/boot/u-boot.img" | cut -f1)"
	fi
	echo
	echo "Successfully built U-Boot for '${build_name}' in $(($(date "+%s") - _start_time)) seconds."
	echo "==============================================================================="

	cleanup
}

main "${@}"

exit 0
